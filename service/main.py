from flask import Flask, request, jsonify

from .enum import ResourceType
from .utils import get_resource
from .exceptions import InvalidUsage

app = Flask(__name__)


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/temperatures')
def get_temperatures():
    return get_resource(request, ResourceType.TEMPERATURE)


@app.route('/speeds')
def get_wind():
    return get_resource(request, ResourceType.WIND)


@app.route('/weather')
def get_weather():
    return get_resource(request, ResourceType.WEATHER)
