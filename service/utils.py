import threading

import requests
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from flask import jsonify

from .enum import ResourceType
from .exceptions import InvalidUsage


def get_resource(request, resource_type):
    """
    Fetchs any type of resources from the backend API
    :param request: The current request object
    :param resource_type: The resource to fetch. Check ResourceType to see all the possible resources
    :return: A json format of the response body
    """
    start_date = request.args.get('start')
    end_date = request.args.get('end')
    if start_date is None or end_date is None:
        raise InvalidUsage('start and end query params are required')

    start_date, end_date = parse(start_date), parse(end_date)
    if start_date >= end_date:
        raise InvalidUsage('end query param should be greater than start query param')
    date = start_date
    threads_list = []
    temperatures = []
    winds = []
    while date <= end_date:
        if resource_type in [ResourceType.TEMPERATURE, ResourceType.WEATHER]:
            thread = threading.Thread(target=api_call, args=(date, 'temperature', temperatures))
            thread.start()
            threads_list.append(thread)

        if resource_type in [ResourceType.WIND, ResourceType.WEATHER]:
            thread = threading.Thread(target=api_call, args=(date, 'windspeed', winds))
            thread.start()
            threads_list.append(thread)

        date += relativedelta(days=1)
    for thread in threads_list:
        thread.join()

    temperatures = sort_resource(temperatures)
    winds = sort_resource(winds)
    result_list = concat_resources(temperatures, winds)

    return jsonify(result_list)


def api_call(date, address, result):
    """
    Makes the API call to the backend service
    :param date: The date of the resource to fetch
    :param address: the hostname + port of the backend service
    :param result: A list where the result will be appended. Mandatory since this function will be run in a thread.
    """
    data = requests.get('http://{}/?at={}'.format(
        address,
        date.isoformat().replace('+00:00', 'Z')
    )).json()

    result.append({
        'date': date.isoformat(),
        'data': data
    })


def sort_resource(resource):
    """
    Sorts the resources, This is required due to the use of threads.
    :return: Sorted resources
    """
    return sorted(resource, key=lambda elem: parse(elem['date']).timestamp())


def concat_resources(temperatures, winds):
    """
    Concats the resources after being sorted.
    :param temperatures: The temperature resources. Empty list if the requested resource is wind
    :param temperatures: The temperature resources. Empty list if the requested resource is temperatures
    :return: Concatenated resources.
    """
    if len(temperatures) == 0:
        return list(map(lambda elem: elem['data'], winds))
    if len(winds) == 0:
        return list(map(lambda elem: elem['data'], temperatures))
    result = []
    for index, wind in enumerate(winds):
        weather = wind.get('data').copy()
        temp = temperatures[index]['data']
        weather.update(temp)
        result.append(weather)
    return result
