import unittest

import requests

from payloads import *


class TemperaturesTests(unittest.TestCase):
    def test_nominal(self):
        response = requests.get('http://service:5000/temperatures?start=2018-08-01T00:00:00Z&end=2018-08-07T00:00:00Z')
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json(), NOMINAL_TEMPERATURES_RESPONSE)

    def test_no_data(self):
        response = requests.get('http://service:5000/temperatures?start=1899-12-31T00:00:00Z&end=1900-01-02T00:00:00Z')
        self.assertListEqual(response.json(), NO_DATA_TEMPERATURES)

    def test_missing_date(self):
        response = requests.get('http://service:5000/temperatures')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"message": "start and end query params are required"})

    def test_wrong_order_dates(self):
        response = requests.get('http://service:5000/temperatures?start=2004-12-31T00:00:00Z&end=1900-01-02T00:00:00Z')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"message": "end query param should be greater than start query param"})


class WindTests(unittest.TestCase):
    def test_nominal(self):
        response = requests.get('http://service:5000/speeds?start=2018-08-01T00:00:00Z&end=2018-08-04T00:00:00Z')
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json(), NOMINAL_WINDS_RESPONSE)


class WeatherTests(unittest.TestCase):
    def test_nominal(self):
        response = requests.get('http://service:5000/weather?start=2018-08-01T00:00:00Z&end=2018-08-04T00:00:00Z')
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.json(), NOMINAL_WEATHER_RESPONSE)
